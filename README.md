# historyposter

[![Build Status](https://drone.datavi.be/api/badges/sneak/historyposter/status.svg)](https://drone.datavi.be/sneak/historyposter)

a daemon that watches local browser history on macOS and POSTs it up to an
API somewhere

i'm going to be using this to send history to an API that uses youtube-dl to
download/cache/archive every single youtube video i view

# author

sneak

https://sneak.berlin

sneak@sneak.berlin

# license

wtfpl
