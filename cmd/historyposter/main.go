package main

import (
	"os"

	"git.eeqj.de/sneak/historyposter/historyposter"
)

// these are filled in at link-time by the build scripts

// Version is the git version of the app
var Version string

// Buildarch contains the architecture it is compiled for
var Buildarch string

func main() {
	os.Exit(historyposter.CLIEntry(Version, Buildarch))
}
