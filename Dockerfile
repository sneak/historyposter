## lint image
FROM golangci/golangci-lint@sha256:9ae3767101cd3468cdaea5b6573dadb358013e05ac38abe37d53646680fd386c AS linter

RUN mkdir -p /go/src/git.eeqj.de/sneak/historyposter
WORKDIR /go/src/git.eeqj.de/sneak/historyposter
COPY ./ ./
RUN golangci-lint run

## build image:
#ARG GO_VERSION=1.15
FROM golang:1.15-alpine AS builder

RUN mkdir /user && \
    echo 'nobody:x:65534:65534:nobody:/:' > /user/passwd && \
    echo 'nobody:x:65534:' > /user/group
RUN apk add --no-cache ca-certificates git bzip2 make gcc libc-dev

#RUN GO111MODULE=on go get github.com/golangci/golangci-lint/cmd/golangci-lint@v1.31.0

RUN mkdir -p /go/src/git.eeqj.de/sneak/historyposter
WORKDIR /go/src/git.eeqj.de/sneak/historyposter

COPY go.mod .
COPY go.sum .
RUN go mod download

COPY ./ ./
#RUN make lint
RUN make build
RUN tar -c /go | bzip2 > /go.tbz2

## output image:
FROM scratch AS final
COPY --from=builder /user/group /user/passwd /etc/
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=builder /go/src/git.eeqj.de/sneak/historyposter/historyposterd /app/historyposterd
COPY --from=builder /go.tbz2 /go.tbz2

WORKDIR /app

ENV PORT 8080
ENV DBURL none

EXPOSE 8080

USER nobody:nobody

ENTRYPOINT ["./historyposterd"]
