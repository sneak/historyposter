module git.eeqj.de/sneak/historyposter

go 1.15

require (
	git.eeqj.de/sneak/goutil v0.0.0-20200922001804-e36581f20570
	git.eeqj.de/sneak/mothership v0.0.0-20200922142345-22de5828bac5
	github.com/k0kubun/pp v3.0.1+incompatible
	github.com/mattn/go-isatty v0.0.12
	github.com/mattn/go-sqlite3 v1.14.3
	github.com/rs/zerolog v1.20.0
	github.com/spf13/viper v1.7.1
)
