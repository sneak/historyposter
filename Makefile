VERSION := $(shell git rev-parse --short HEAD)
BUILDTIME := $(shell date -u '+%Y-%m-%dT%H:%M:%SZ')
BUILDTIMEFILENAME := $(shell date -u '+%Y%m%d-%H%M%SZ')
BUILDTIMETAG := $(shell date -u '+%Y%m%d%H%M%S')
BUILDUSER := $(shell whoami)
BUILDHOST := $(shell hostname -s)
BUILDARCH := $(shell uname -m)

FN := historyposter

UNAME_S := $(shell uname -s)

GOLDFLAGS += -X main.Version=$(VERSION)
GOLDFLAGS += -X main.Buildarch=$(BUILDARCH)

# osx can't statically link apparently?!
ifeq ($(UNAME_S),Darwin)
	GOFLAGS := -ldflags "$(GOLDFLAGS)"
endif

ifneq ($(UNAME_S),Darwin)
	GOFLAGS = -ldflags "-linkmode external -extldflags -static $(GOLDFLAGS)"
endif

default: debug

commit: fmt lint
	git commit -a

fmt:
	#go fmt ./...
	goimports -l -w .
	golangci-lint run --fix

lint:
	golangci-lint run
	sh -c 'test -z "$$(gofmt -l .)"'

debug: build
	-mkdir -p ./.env.d
	echo 1 > ./.env.d/HISTORYPOSTER_DEBUG
	GOTRACEBACK=all envdir ./.env.d ./$(FN)d 2>&1 | tee -a debug.log

debugger:
	cd cmd/historyposter && dlv debug main.go

run: build
	./$(FN)d

clean:
	-rm ./$(FN)d debug.log

build: ./$(FN)d

docker:
	docker build .

go-get:
	cd cmd/$(FN) && go get -v

./$(FN)d: */*.go cmd/*/*.go go-get
	cd cmd/$(FN) && go build -o ../../$(FN)d $(GOFLAGS) .

.PHONY: build fmt test is_uncommitted docker dist hub upload-docker-image clean run rundebug default build-docker-image-dist
